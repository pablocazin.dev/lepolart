<header>
    <div class="headtop">
        <div class="logo"><img src="./assets/logo 120x120.gif"></img></div>
        <div class="titre">
            <h1>LEPOL'ART</h1>
            <span>Association sous loi 1901</span>
        </div>
        <div class="social">
            <a href="https://www.instagram.com/?hl=fr"><img src="./assets/insta.png" alt="logoInstagram" width="80px"
                    height="80px"></a>
            <a href="https://fr.linkedin.com/"><img src="./assets/linke.png" alt="logoLinkedin" width="75px"
                    height="75px"></a>
            <a href="https://fr-fr.facebook.com/"><img src="./assets/faceb.png" alt="logoFacebook" width="80px"
                    height="80px"></a>
        </div>
    </div>
    <div id="headbarre"></div>
    <div class="headbottom">
        <nav>
            <h3>INTENTIONS</h3>
            <h3>MENU ORCHESTRE PARTICIPATIF</h3>
            <h3>PRODUCTIONS</h3>
            <h3>ADHESION</h3>
        </nav>
    </div>

</header>