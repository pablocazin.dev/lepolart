<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Présentation de LEPOL'ART">
    <link rel="stylesheet" href="./style/style.css">
    <title>Menu Orchestre Participatif</title>
</head>

<body>
    <?php 
    include 'header.php'
    ?>
    <main>
        <div class="wrapper">
            <div class="container-full">
                <div class="container">
                    <div class="box-title">
                        <a href="mop_detail.php"><img src="./assets/cotecuisine.jpeg" alt="côté cuisine"
                                class="img-cote"></a>
                        <span class="title">Côté Cuisine</span>
                    </div>
                    <div class="box-text">
                        <h2 class="sub-title">« Une heure qui a du goût »</h2>
                        <p class="p-header">WORSHOPS
                            pluridisciplinaire
                            (= Stage-Atelier d’expérimentation)</p>
                        <p class="p-footer">Description courte...
                            Click: page détaillée</p>
                    </div>
                </div>
                <div class="container">
                    <div class="box-title">
                        <a href="mop_detail.php"><img src="./assets/cotejardin.jpeg" alt="côté jardin"
                                class="img-cote"></a>
                        <span class="title">Côté Jardin</span>
                    </div>
                    <div class="box-text">
                        <h2 class="sub-title">« A vos plantes prêts, plantons au jardin public ! »</h2>
                        <p class="p-header">Description courte...
                            Click: page détaillée</p>
                        <p class="p-footer">Inscription ?
                            => sur la page dédiée</p>
                    </div>
                </div>
                <div class="container">
                    <div class="box-title">
                        <a href="mop_detail.php"><img src="./assets/cotecour.jpeg" alt="côté cour" class="img-cote"></a>
                        <span class="title">Coté Cour</span>
                    </div>
                    <div class="box-text">
                        <h2 class="sub-title">« Evenements »</h2>
                        <p class="p-header">FESTIVAL DE LA PERFORMANCE CONTEMPORAINE INTERNATIONALE</p>
                        <p class="p-footer">filmée et retransmise en direct</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php 
    include 'footer.php'
    ?>
</body>

</html>