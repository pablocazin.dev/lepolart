<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Présentation de LEPOL'ART">
    <link rel="stylesheet" href="./style/style_prod.css">
    <link rel="stylesheet" href="./style/style.css">

    <title>Productions</title>
</head>

<body>
    <?php 
    include 'header.php'
    ?>
    <main>
        <div class="wrapper2">
            <!-- Titre page  -->
            <div id="headbarre"></div>
            <h1 class="title-page">SUPPORT DE CRÉATION EN WORK IN PROGRESS, en quête de diffusion permanente</h1>
            <!-- Zone Images / 2 lignes -->

            <?php
                // On détermine sur quelle page on se trouve
                if(isset($_GET['page']) && !empty($_GET['page'])){
                    $currentPage = (int) strip_tags($_GET['page']);
                }else{
                    $currentPage = 1;
                }

                // On se connecte à là base de données
                include data.php;

                // On récupère les productions
                $idsProd = getEvtsIds("productions");

                $nbProd = count($idsProd);

                // On détermine le nombre d'articles par page
                $parPage = 8;

                // On calcule le nombre de pages total
                $nbPages = ceil($nbProd / $parPage);

                // Calcul du 1er article de la page
                $premier = ($currentPage * $parPage) - $parPage;

                echo "</p>$productions";
                echo "</p>$nbProd";
                echo "</p>$parPage";
                echo "</p>$nbPages";
                echo "</p>$premier";      
            ?>

            <div class="col">
                <div class="ligne axe1-sp-around padding-V10">
                    <!-- foreach 4 img  -->
                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>
                </div>

                <div class="ligne axe1-sp-around padding-V10">
                    <!-- foreach 4 img  -->
                    <!-- foreach 4 img  -->
                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>
                </div> <!-- Fin ligne 2 -->
            </div> <!-- Fin col 2 lignes -->
            <div>
                <nav>
                    <ul class="ligne">
                        <li ><a href="#"> < </a></li>
                        <li ><a href="#"> 1 </a></li>
                        <li ><a href="#"> > </a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </main>



    <?php 
    include 'footer.php'
    ?>
</body>

</html>