<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Présentation des menus Orchestre participatif">
    <link rel="stylesheet" href="./style/style.css">
    <title>Menu Orchestre Participatif</title>
</head>

<body>
    <?php 
    include 'header.php'
    ?>
    <main>

    </main>
    <?php 
    include 'footer.php'
    ?>
</body>

</html>