<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>LEPOL'ART</title>
</head>

<body>
    <header>
        <div>
            <div class="logo"><img src="./assets/logo 120x120.gif"></img></div>
            <div>
                <h1>LEPOL'ART</h1>
                <span>Association sous loi 1901</span>
            </div>
            <div class="social">
                <a href="https://www.instagram.com/?hl=fr"><img src="./assets/insta.png" alt="logoInstagram"></a>
                <a href="https://fr.linkedin.com/"><img src="./assets/linke.png" alt="logoLinkedin"></a>
                <a href="https://fr-fr.facebook.com/"><img src="./assets/faceb.png" alt="logoFacebook"></a>
            </div>
        </div>
    </header>
 
</body>

</html>