<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Présentation de LEPOL'ART">
    <link rel="stylesheet" href="./style/style_prod.css">
    <link rel="stylesheet" href="./style/style.css">

    <title>Productions</title>
</head>

<body>
    <?php 
    include 'header.php'
    ?>
    <main>
        <div class="wrapper2">
            <!-- Titre page  -->
            <div id="headbarre"></div>
            <h1 class="title-page">SUPPORT DE CRÉATION EN WORK IN PROGRESS, en quête de diffusion permanente</h1>
            <!-- Zone Images / 2 lignes -->
            <div class="col">
                <div class="ligne axe1-sp-around padding-V10">
                    <!-- foreach 4 img  -->
                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>
                </div>

                <div class="ligne axe1-sp-around padding-V10">
                    <!-- foreach 4 img  -->
                    <!-- foreach 4 img  -->
                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>

                    <div>
                        <a href="mop_detail.php">
                        <img src="./assets/cotecuisine.jpeg" alt="img production" class="img-cote2">
                        </a>
                    </div>
                </div> <!-- Fin ligne 2 -->
            </div> <!-- Fin col 2 lignes -->
            <div class="ligne">
                <nav>
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#">Précédent</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">Suivant</a></li>
                    </ul>
                </nav>
                
            </div>
        </div>
    </main>



    <?php 
    include 'footer.php'
    ?>
</body>

</html>