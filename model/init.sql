DROP DATABASE IF EXISTS polart;
CREATE DATABASE polart;
USE polart;

GRANT ALL PRIVILEGES ON polart.* TO 'admin'@'localhost';
-- GRANT ALL PRIVILEGES ON polart.* TO 'u_cyril'@'localhost';

flush privileges;

CREATE TABLE info (
    section1 VARCHAR(50),
    section2 VARCHAR(50),
    section3 VARCHAR(50),
    section4 VARCHAR(50),
    url_adhesion VARCHAR(250),
    description_asso TEXT
);

CREATE TABLE administrateur (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    pseudo VARCHAR(50),
    mdp VARCHAR(100)
);

CREATE TABLE cat (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(250),
    genre ENUM("evenements", "mop", "productions"),
    img VARCHAR(255)
);

CREATE TABLE evenements (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(100),
    lien VARCHAR(255)
);

-- CREATE TABLE evenements_cat plutot 
CREATE TABLE evenements_cat (
    id_evenement INT UNSIGNED,
    id_cat INT UNSIGNED,
    UNIQUE (id_evenement, id_cat),
    FOREIGN KEY (id_evenement) REFERENCES evenements(id),
    FOREIGN KEY (id_cat) REFERENCES cat(id)
);

CREATE TABLE images (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    fichier VARCHAR(100)
);

CREATE TABLE evenements_images (
    id_evenement INT UNSIGNED,
    id_image INT UNSIGNED,
    ordre INT UNSIGNED,
    UNIQUE (id_evenement, id_image),
    FOREIGN KEY (id_evenement) REFERENCES evenements(id),
    FOREIGN KEY (id_image) REFERENCES images(id)
);

CREATE TABLE contents (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    titre VARCHAR(100),
    texte VARCHAR(350)
);

CREATE TABLE evenements_contents (
    id_evenement INT UNSIGNED,
    id_content INT UNSIGNED,
    ordre INT UNSIGNED,
    UNIQUE (id_evenement, id_content),
    FOREIGN KEY (id_evenement) REFERENCES evenements(id),
    FOREIGN KEY (id_content) REFERENCES contents(id)
);

CREATE TABLE artistes (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(30),
    prenom VARCHAR(30),
    url VARCHAR(255)
);


