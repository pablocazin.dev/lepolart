
-- Use script: sudo mysql < fill.sql;

-- DATABASE:    polart
-- TYPE ENUM:   
-- TABLE:       
--              
-- -------------------------------------------------------------
-- unix> sudo mysql
-- mysql> ...
-- show databases;
-- use polart;
-- show tables;
-- describe table...;
-- select * from table...;
-- quit;
-- exit;
-- -------------------------------------------------------------

USE polart;

-- TABLE Produit  -----------------------------

INSERT INTO evenements (nom)
    VALUES 
            ( "Production 01"),
            ( "Production 02"),
            ( "Production 03"),
            ( "Production 04"),
            ( "Production 05"),
            ( "Production 06"),
            ( "Production 07"),
            ( "Production 08"),
            ( "Production 09"),
            ( "Production 10"),
            ( "Production 11"),
            ( "Production 12"),
            ( "Production 13"),
            ( "Production 14"),
            ( "Production 15"),
            ( "Production 16"),
            ( "Production 17");


INSERT INTO images (fichier)
    VALUES 
            ( "prod 01a cotejardin.jpg"),
            ( "prod 01b cotejardin.jpg"),
            ( "prod 01c cotejardin.jpg"),

            ( "prod 02a cotecuisine.jpg"),
            ( "prod 02b cotecuisine.jpg"),
            ( "prod 02c cotecuisine.jpg"),

            ( "prod 00 cotecour copy 1.jpg"),
            ( "prod 00 cotecour copy 2.jpg"),
            ( "prod 00 cotecour copy 3.jpg"),
            ( "prod 00 cotecour copy 4.jpg"),
            ( "prod 00 cotecour copy 5.jpg"),
            ( "prod 00 cotecour copy 6.jpg"),
            ( "prod 00 cotecour copy 7.jpg"),
            ( "prod 00 cotecour copy 8.jpg"),
            ( "prod 00 cotecour copy 9.jpg"),
            ( "prod 00 cotecour copy 10.jpg"),
            ( "prod 00 cotecour copy 11.jpg"),
            ( "prod 00 cotecour copy 12.jpg"),
            ( "prod 00 cotecour copy 13.jpg"),
            ( "prod 00 cotecour copy 14.jpg"),
            ( "prod 00 cotecour copy 15.jpg"),
            ( "prod 00 cotecour copy 16.jpg");

INSERT INTO evenements_images (id_evenement, id_image, ordre)
    VALUES  
            (1, 1, 1),  -- "Production 01"          -- 3 images
            (1, 2, 2),
            (1, 3, 3),
            (2, 4, 1),  -- "Production 02"          -- 3 images
            (2, 5, 2),
            (2, 6, 3),
            (3, 7, 1),  -- "Production 03"          -- 1 image
            (4, 8, 1),  -- "Production 04 etc..."   -- 1 image
            (5, 9, 1),
            (6, 10, 1),
            (7, 11, 1),
            (8, 12, 1),
            (9, 13, 1),
            (10, 14, 1),
            (11, 15, 1),
            (12, 16, 1),
            (13, 17, 1),
            (14, 18, 1),
            (15, 19, 1),
            (16, 20, 1),
            (17, 21, 1);

INSERT INTO cat (nom, genre, img) 
    VALUES 
        ('coté jardin', 'mop', "assets/cotejardin.jpeg"), 
        ('coté cours', 'mop', "assets/cotecour.jpeg"),
        ('coté cuisine', 'mop', "assets/cotecuisine.jpeg");

INSERT INTO cat (genre) 
    VALUES 
        -- id 1 2 3: cat = mop
        -- id 4 ... +17 = productions
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions"),
        ("productions");

INSERT INTO evenements_cat (id_evenement, id_cat)
    VALUES
        (1, 1), -- Evt_1 = Cat 1    MOP
        (2, 2), -- Evt_2 =    ...   MOP
        (3, 3), -- Evt_3 =          MOP

        (1, 4), -- Evt_1 = Cat 4    Prod
        (2, 5), -- Evt_2 =    ...   Prod
        (3, 6), -- Evt_3 =          ...

        (4, 7), 
        (5, 8), 
        (6, 9), 
        (7, 10), 
        (8, 11), 
        (9, 12), 
        (10, 13), 
        (11, 14), 
        (12, 15), 
        (13, 16), 
        (14, 17),
        (15, 18),
        (16, 19),
        (17, 20);
















