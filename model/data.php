<?php 

include 'debug.php';

$host = 'localhost';
$db   = 'polart';
$user = 'admin';
$pass = 'admin'; 
// $user = 'u_cyril';

// CONNEXION BDD =====================================================

try {
    $pdo = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "connexion réussie <br>";
} catch(PDOException $e) {
    echo "Erreur : " . $e->getMessage();
}

// CRUD : Create ===================================================

// CRUD : Read =====================================================

function getInfo() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM info;");
    return $req->fetchAll();
}

function getArtiste() {
    global $pdo;
    $req = $pdo->query('SELECT * FROM artistes;');
    return $req->fetchAll();
}

function getMOP() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM cat WHERE genre = 'mop';");
    return $req->fetchAll();
}

function getMopEvents() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM evenements WHERE ;");
}

function getProd() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM cat WHERE genre = 'productions';");
    return $req->fetchAll();
}

// faire une fonction qui prends en parametre un nom/id, et qui renvoie toutes
// les informations relatives au projet, images, textBlock, titre, ordre

function getEvtsIds($genre) { //!!!
    global $pdo;
    $sql = "SELECT id_evenement FROM evenements_cat
            INNER JOIN cat
            ON cat.id = evenements_cat.id_cat
            WHERE cat.genre = '$genre'
            "; 
    $req = $pdo->prepare($sql);
    $req->execute();
    return $req->fetchAll();
}


function getEvt($id_evt) {
    global $pdo;
    // $sql = "SELECT * FROM evenements WHERE id = $id_evt";
    $req = $pdo->prepare($sql);
    $req->execute();
    return $req->fetchAll();
}

function getEvtImage01($id_evt) {
    global $pdo;
    $sql = "SELECT * FROM images
            INNER JOIN evenements_images
            ON images.id = evenements_images.id_images
            WHERE evenements_images.id_evenements = $id_evt
                AND evenements_images.ordre = 1
            "; 
    $req = $pdo->prepare($sql);
    $req->execute();
    // return $req->fetchAll();
    return $req->fetch();
}
function getEvtAllImages($id_evt) {
    global $pdo;
    $sql = "SELECT * FROM images
            INNER JOIN evenements_images
            ON images.id = evenements_images.id_images
            WHERE evenements_images = $id_evt
            ";  
    $req = $pdo->prepare($sql);
    $req->execute();
    return $req->fetchAll();
}

// CRUD : Update =====================================================

// CRUD : Delete =====================================================

?>

